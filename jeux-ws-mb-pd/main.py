# -*- coding: utf-8 -*-

#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import webapp2
import json
import datetime
import logging
import traceback

from google.appengine.ext import ndb
from google.appengine.ext import db
from datetime import datetime

from models import Game, Hero


def serialiser_pour_json(objet):
    """ Permet de sérialiser les dates et heures pour transformer
        un objet en JSON.

        Args:
            objet (obj): L'objet à sérialiser.

        Returns:
            obj : Si c'est une date et heure, retourne une version sérialisée
                  selon le format ISO (str); autrement, retourne l'objet
                  original non modifié.
    """
    #if isinstance(objet, datetime.datetime):
    if isinstance(objet, datetime):
        # Pour une date et heure, on retourne une chaîne
        # au format ISO (sans les millisecondes).
        return objet.replace(microsecond=0).isoformat()
    elif isinstance(objet, datetime.date):
        # Pour une date, on retourne une chaîne au format ISO.
        return objet.isoformat()
    else:
        # Lève une exception si ce n'est pas une date.
        raise TypeError()



class MainPageHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write('Hello world!')
       
class GameHandler(webapp2.RequestHandler):
    def get(self, idJeu=None):
        try:
            if idJeu is not None:
                cle = ndb.Key('Game', int(idJeu))
                jeu = cle.get()

                if (jeu is None):
                    self.error(404)
                    return
                jeu_dict = jeu.to_dict()
                jeu_dict['id'] = jeu.key.id()
                json_data = json.dumps(jeu_dict, default=serialiser_pour_json)
            else:

                motCle = self.request.get('motCle').strip()
                hero = self.request.get('hero').strip()

                releaseDateMin = self.request.get('releaseDateMin').strip()
                releaseDateMax = self.request.get('releaseDateMax').strip()
  
  
                list_jeu = []
                # requete = Game.query().fetch(20)

                #&releaseDateMax=01/01/1993

                #requete = Game.query(Game.releaseDate >= releaseDateMin)
                #requete = Game.query(Game.releaseDate <= releaseDateMax)

                #Ça retourne les 5 premiers entre les 2 dates
                #requete = Game.query(Game.releaseDate >= releaseDateMin, Game.releaseDate <= releaseDateMax).fetch(5)

                #what is that thing -->


                req = Game.query()

                if  motCle is not "":
                    req = req.filter(Game.gameTitle == motCle)

                if releaseDateMin is not "":
                    # self.response.write(releaseDateMin)
                    # self.response.write("-")
                    releaseDateMin = datetime.strptime(self.request.get('releaseDateMin').strip(),  "%m/%d/%Y")
                    req = req.filter(Game.releaseDate >= releaseDateMin)

                if releaseDateMax is not "":
                    releaseDateMax = datetime.strptime(self.request.get('releaseDateMax').strip(),  "%m/%d/%Y")
                    req = req.filter(Game.releaseDate <= releaseDateMax)

                
                
                req = req.fetch(20)
                #req = req.orders(Game.gameTitle)


                # if releaseDateMin is not None and releaseDateMax is not None:
                #     requete = Game.query(Game.releaseDate >= releaseDateMin, Game.releaseDate <= releaseDateMax).fetch(5)
                
                    
                    #requete = Game.query().fetch(20)
                #     self.response.write("Hello World")
                # else:
                #     requete = Game.query().fetch(20)
                

                #localhost:12080/game?releaseDateMin=01/01/1989&releaseDateMax=01/01/1993

                for jeu in req:
                    jeu_dict = jeu.to_dict()
                    jeu_dict['id'] = jeu.key.id()
                    list_jeu.append(jeu_dict)

                json_data = json.dumps(list_jeu, default=serialiser_pour_json)

            self.response.set_status(200)
            self.response.headers['Content-Type'] = ('application/json;' +
                                                            ' charset=utf-8')
            self.response.out.write(json_data)
        
        except (db.BadValueError, ValueError, KeyError):
            logging.error('%s', traceback.format_exc())
            self.error(400)

        except Exception:
            logging.error('%s', traceback.format_exc())
            self.error(500)

            
    def put(self, idJeu):
        try:
            #Creation de la cle selon son id
            cle = ndb.Key('Game', int(idJeu))

            #On va chercher le game lie a la cle
            game = cle.get()

            if game is None:
                # L'entité n'existe pas, elle sera créée (201 Created).
                game = Game(key=cle)
                status = 201
            else:
                status = 200
            
            game_dict_in = json.loads(self.request.body)

            game.gameTitle = game_dict_in['gameTitle'].strip()
            game.platform = game_dict_in['platform'].strip()
            #game.releaseDate = game_dict_in['releaseDate'].strip()

            game.put()
            self.response.set_status(status)
            game_dict = game._to_dict()

            #  WARNING
            game_dict['idJeu'] = game.key.id()

            game_json = json.dumps(game_dict, default=serialiser_pour_json)
            self.response.write(game_json)

        # Exceptions en lien avec les données fournies (liées à la requête).
        except (db.BadValueError, ValueError, KeyError):
            logging.error('%s', traceback.format_exc())
            # Bad Request.
            self.error(400)

        # Exceptions graves lors de l'exécution du code
        # (non liées à la requête).
        except Exception:
            logging.error('%s', traceback.format_exc())
            # Internal Server Error.
            self.error(500)

    def delete(self, idJeu):
        try:
            if idJeu is not None:
                cle = ndb.Key('Game', idJeu)
                ndb.delete_multi(Game.query(ancestor=cle).fetch(keys_only=True)) # un delete_multi ici?? INTRU! ***
                cle.delete()

        except (db.BadValueError, ValueError, KeyError):
            logging.error('%s', traceback.format_exc())
            self.error(400)

        except Exception:
            logging.error('%s', traceback.format_exc())
            self.error(500)

class HeroHandler(webapp2.RequestHandler):
    def get(self, idJeu):
        try:
            cle_jeu = ndb.Key('Game', int(idJeu))
            
            if (cle_jeu.get() is None):
                self.error(404)
                return          
            else:
               # hero = Hero.query()

               # for test in hero:
               #     self.response.write(test)

                list_heroes = []
                for hero in Hero.query(ancestor=cle_jeu).fetch():
                    #self.response.write(hero.name)
                    hero_dict = hero.to_dict()
                    hero_dict['id'] = hero.key.id()
                    hero_dict['parent-id'] = cle_jeu.id()
                    list_heroes.append(hero_dict)

                json_data = json.dumps(list_heroes, default=serialiser_pour_json)

                self.response.set_status(200)
                self.response.headers['Content-Type'] = ('application/json;' +
                                                        ' charset=utf-8')
                self.response.out.write(json_data)
        
        except (db.BadValueError, ValueError, KeyError):
            logging.error('%s', traceback.format_exc())
            self.error(400)

        except Exception:
            logging.error('%s', traceback.format_exc())
            self.error(500)


class DatastoreHandler(webapp2.RequestHandler):
    ###################################
    #A EFFACER
    ###################################
    def get(self):
        list_jeu = []
        requete = Game.query()

        # for jeu in requete:
        #     self.response.write(jeu)

        for jeu in requete:
            jeu_dict = jeu.to_dict()
            jeu_dict['id'] = jeu.key.id()
            list_jeu.append(jeu_dict)

        json_data = json.dumps(list_jeu, default=serialiser_pour_json)

        self.response.set_status(200)
        self.response.headers['Content-Type'] = ('application/json;' +
                                                            ' charset=utf-8')
        self.response.out.write(json_data)
    #####################################
    ######################################


    def delete(self):
        #self.response.write("MARCHE CALISS")
        #ndb.delete_multi(Hero.query().fetch(keys_only=True))
        try:
            ndb.delete_multi(Hero.query().fetch(keys_only=True))
            ndb.delete_multi(Game.query().fetch(keys_only=True))
        except (db.BadValueError, ValueError, KeyError):
            #logging.error('%s', traceback.format_exc())
            self.error(400)
        except Exception:
           # logging.error('%s', traceback.format_exc())
            self.error(500)
    def post(self):
        with open('jeux.json') as data_file:    
            data = json.load(data_file)

        for val in data:
            #Cree un objet game
            dateLancement = val['ReleaseDate']
            
            if not dateLancement:
                dateLancement = "01/01/0001"

            releaseDateFormat = datetime.strptime(dateLancement, "%m/%d/%Y")
            game = Game(gameTitle=val['GameTitle'], releaseDate=releaseDateFormat, platform=val['Platform'])

            #On ajoute le game au datastore
            game.put()

            #La cle pour les heros
            cle_parent_game = ndb.Key('Game', game.key.id())

            tabHero = []
            if 'Heroes' in val:
                tabHero.append(val['Heroes'])
            
                for elem in tabHero:
                    hero = Hero(parent=cle_parent_game)
                    hero.name = str(elem)

                    #On ajoute le hero au datastore
                    hero.put()                


app = webapp2.WSGIApplication(
    [
        webapp2.Route(r'/',
            handler=MainPageHandler,
            methods=['GET']), 
        webapp2.Route(r'/game',
            handler=GameHandler,
            methods=['GET']),
        webapp2.Route(r'/game/<idJeu>',
            handler=GameHandler,
            methods=['GET','PUT', 'DELETE']),
        webapp2.Route(r'/game/<idJeu>/heroes',
            handler=HeroHandler,
            methods=['GET']),
        webapp2.Route(r'/datastore',
            handler=DatastoreHandler,
            methods=['POST', 'DELETE', 'GET'])
            
    ], 
    debug=True)
