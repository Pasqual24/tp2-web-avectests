# -*- coding: utf-8 -*-

from google.appengine.ext import ndb

class Game(ndb.Model):
    #id = ndb.StringProperty(required=True)
    gameTitle = ndb.StringProperty(required=True)
    releaseDate = ndb.DateTimeProperty()
    platform = ndb.StringProperty(required=True)

#Aura "Game" comme entite parent
class Hero(ndb.Model):
    #idGame = ndb.StringProperty(required=True)
    name = ndb.StringProperty(required=True)

