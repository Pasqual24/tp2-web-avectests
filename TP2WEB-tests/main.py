# -*- coding: utf-8 -*-

import webapp2
import datetime
import json
# Pour faire des requêtes vers le service Web;
# ce module doit être ajouté au projet.
# import requests
# Moteur de modèles (template engine).
import jinja2
import os

from google.appengine.api import urlfetch

# Configuration de Jinja2.
template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(
                        loader=jinja2.FileSystemLoader(template_dir),
                        autoescape=True)

# Chargement du modèle (template).
template = jinja_env.get_template("index.html")


def serialiser_pour_json(objet):
    """ Permet de sérialiser les dates et heures pour transformer
        un objet en JSON.

        Args:
            objet (obj): L'objet à sérialiser.

        Returns:
            obj : Si c'est une date et heure, retourne une version sérialisée
                  selon le format ISO (str); autrement, retourne l'objet
                  original non modifié.
    """
    if isinstance(objet, datetime.datetime):
        # Pour une date et heure, on retourne une chaîne
        # au format ISO (sans les millisecondes).
        return objet.replace(microsecond=0).isoformat()
    elif isinstance(objet, datetime.date):
        # Pour une date, on retourne une chaîne au format ISO.
        return objet.isoformat()
    else:
        # Lève une exception si ce n'est pas une date.
        raise TypeError()

# S   É   P   A   R   A   T   E   U   R
###########################################################################################################################
###########################################################################################################################
###########################################################################################################################
############################################################lol############################################################
###########################################################################################################################
###########################################################################################################################
###########################################################################################################################
# S   É   P   A   R   A   T   E   U   R

class MainPageHandler(webapp2.RequestHandler):

    def creer_dict_test(self, titre, url, methode, dict_en_tetes,
                        contenu_json, list_codes_statut, reponse):
        """ Permet de créer un dictionnaire contenant toute l'information
            sur un test.

            Args:
                titre (str): Titre associé au test.
                url (str): URL de la requête.
                methode (str): Méthode HTTP utilisée par la requête.
                dict_en_tetes (dict): En-têtes HTTP de la requête.
                contenu_json (str): Contenu de la requête en JSON.
                list_codes_statut (list): Codes de statut de la réponse
                                          signifiant un succès.
                reponse (obj): Réponse associée à la requête.

            Returns:
                dict : Un dictionnaire contenant toute l'information
                       sur le test.
        """
        if contenu_json != 'null':
            requete_contenu = contenu_json
        else:
            requete_contenu = None
        # Dictionnaire contenant les informations sur la requête.
        dict_requete = {'url': url, 'methode': methode,
                        'enTetes': dict_en_tetes, 'contenu': requete_contenu}

        # Y a-t-il un contenu dans la réponse ?
        if reponse.content is not None and reponse.content != '':
            reponse_contenu = reponse.content
        else:
            reponse_contenu = None
        # Dictionnaire contenant les info sur la réponse.
        dict_reponse = {'statut': reponse.status_code,
                        'contenu': reponse_contenu}

        # Est-ce que le code de statut de la réponse est un code
        # correspondant à un succès ?
        succes = True if reponse.status_code in list_codes_statut else False

        # Dictionnaire contenant les info sur le test.
        dict_test = {'titre': titre, 'req': dict_requete, 'rep': dict_reponse,
                     'succes': succes}

        return dict_test

    def get(self):
        self.response.write('Hello world!asddsd')

        # URL de la racine du service Web.
        # Version locale.
        url_base = "http://localhost:12080"

        # Liste contenant les informations sur tous les tests effectués.
        list_tests = []

        ###########################################
        # Ré-initialisation de la base de données #
        ###########################################
        # Notez le 'u' devant la chaîne.
        titre = u'Ré-initialisation de la base de données'
        # Ou bien :
        # titre = 'Ré-initialisation de la base de données'.decode('utf-8')

        # Informations pour créer la requête.
        url = url_base + '/datastore'
        methode = 'DELETE'
        dict_en_tetes = {}
        dict_contenu = None
        # Conversion du dictionnaire en chaîne JSON.
        contenu_json = json.dumps(dict_contenu, default=serialiser_pour_json)

        # Codes de statut de la réponse signifiant un succès.
        list_codes_statut = [200, 204]

        # Envoi de la requête DELETE.
        # reponse = requests.delete(url, headers=dict_en_tetes)
        reponse = urlfetch.fetch(url, method=urlfetch.DELETE,
                                 headers=dict_en_tetes,
                                 follow_redirects=False, deadline=None)

        # Création d'un dictionnaire contenant toute l'information
        # sur le test (requête, réponse et résultat).
        dict_test = self.creer_dict_test(titre, url, methode,
                                         dict_en_tetes, contenu_json,
                                         list_codes_statut, reponse)

        # Ajout du test à la liste de tests.
        list_tests.append(dict_test)

        ###############################
        # TEST-1 : Créer un jeu #
        ###############################
        titre = u'TEST-1 : Créer un jeu'

        # Informations pour créer la requête.
        url = url_base + '/game/242424'
        methode = 'PUT'
        dict_en_tetes = {'Content-Type': 'application/json; charset=utf-8',
                         'Accept': 'application/json'}
        dict_contenu = {'GameTitle': 'Le Petit Poney', 'ReleaseDate': '01/01/2012',
        'Platform' : 'Nintendo Game Boy Advance'}
        # Conversion du dictionnaire en chaîne JSON.
        contenu_json = json.dumps(dict_contenu, default=serialiser_pour_json)

        # Codes de statut de la réponse signifiant un succès.
        list_codes_statut = [201]

        # Envoi de la requête PUT.
        # reponse = requests.put(url, headers=dict_en_tetes, data=contenu_json)
        reponse = urlfetch.fetch(url, payload=contenu_json,
                                 method=urlfetch.PUT, headers=dict_en_tetes,
                                 follow_redirects=False, deadline=None)

        # Création d'un dictionnaire contenant toute l'information
        # sur le test (requête, réponse et résultat).
        dict_test = self.creer_dict_test(titre, url, methode,
                                         dict_en_tetes, contenu_json,
                                         list_codes_statut, reponse)

        # Ajout du test à la liste de tests.
        list_tests.append(dict_test)

        
        ###############################
        # TEST-2 : Accéder à un jeu #
        ###############################
        titre = u'TEST-2 : Accéder à un jeu'

        # Informations pour créer la requête.
        url = url_base + '/game/242424'
        methode = 'GET'
        dict_en_tetes = {'Content-Type': 'application/json; charset=utf-8',
                         'Accept': 'application/json'}
        dict_contenu = {}
        # Conversion du dictionnaire en chaîne JSON.
        contenu_json = json.dumps(dict_contenu, default=serialiser_pour_json)

        # Codes de statut de la réponse signifiant un succès.
        list_codes_statut = [200]

        # Envoi de la requête PUT.
        # reponse = requests.put(url, headers=dict_en_tetes, data=contenu_json)
        reponse = urlfetch.fetch(url, payload=contenu_json,
                                 method=urlfetch.PUT, headers=dict_en_tetes,
                                 follow_redirects=False, deadline=None)

        # Création d'un dictionnaire contenant toute l'information
        # sur le test (requête, réponse et résultat).
        dict_test = self.creer_dict_test(titre, url, methode,
                                         dict_en_tetes, contenu_json,
                                         list_codes_statut, reponse)

        # Ajout du test à la liste de tests.
        list_tests.append(dict_test)

        ###############################
        # TEST-3 : Modifier un jeu #
        ###############################
        titre = u'TEST-3 : Modifier un jeu'

        # Informations pour créer la requête.
        url = url_base + '/game/242424'
        methode = 'PUT'
        dict_en_tetes = {'Content-Type': 'application/json; charset=utf-8',
                         'Accept': 'application/json'}
        dict_contenu = {
            "GameTitle": "Le Petit Poney: La Revanche", 
            "ReleaseDate": "01/01/2014",
            "Platform" : "Nintendo Game Boy Advance 2"
        }

        # Conversion du dictionnaire en chaîne JSON.
        contenu_json = json.dumps(dict_contenu, default=serialiser_pour_json)

        # Codes de statut de la réponse signifiant un succès.
        list_codes_statut = [200]

        # Envoi de la requête PUT.
        # reponse = requests.put(url, headers=dict_en_tetes, data=contenu_json)
        reponse = urlfetch.fetch(url, payload=contenu_json,
                                 method=urlfetch.PUT, headers=dict_en_tetes,
                                 follow_redirects=False, deadline=None)

        # Création d'un dictionnaire contenant toute l'information
        # sur le test (requête, réponse et résultat).
        dict_test = self.creer_dict_test(titre, url, methode,
                                         dict_en_tetes, contenu_json,
                                         list_codes_statut, reponse)

        # Ajout du test à la liste de tests.
        list_tests.append(dict_test)

        ###############################
        # TEST-4 :Accéder à un jeu modifié #
        ###############################
        titre = u'TEST-4 :Accéder à un jeu modifié'

        # Informations pour créer la requête.
        url = url_base + '/game/242424'
        methode = 'GET'
        dict_en_tetes = {'Content-Type': 'application/json; charset=utf-8',
                         'Accept': 'application/json'}
        dict_contenu = {}

        # Conversion du dictionnaire en chaîne JSON.
        contenu_json = json.dumps(dict_contenu, default=serialiser_pour_json)

        # Codes de statut de la réponse signifiant un succès.
        list_codes_statut = [200]

        # Envoi de la requête PUT.
        # reponse = requests.put(url, headers=dict_en_tetes, data=contenu_json)
        reponse = urlfetch.fetch(url, payload=contenu_json,
                                 method=urlfetch.PUT, headers=dict_en_tetes,
                                 follow_redirects=False, deadline=None)

        # Création d'un dictionnaire contenant toute l'information
        # sur le test (requête, réponse et résultat).
        dict_test = self.creer_dict_test(titre, url, methode,
                                         dict_en_tetes, contenu_json,
                                         list_codes_statut, reponse)

        # Ajout du test à la liste de tests.
        list_tests.append(dict_test)

        ###################################
        # TEST-5 : Supprimer un jeu #
        ###################################
        titre = u'TEST-5 : Supprimer un jeu'

        # Informations pour créer la requête.
        url = url_base + '/games/2424'
        methode = 'DELETE'
        dict_en_tetes = {}
        dict_contenu = None
        # Conversion du dictionnaire en chaîne JSON.
        contenu_json = json.dumps(dict_contenu, default=serialiser_pour_json)

        # Codes de statut de la réponse signifiant un succès.
        list_codes_statut = [200, 204]

        # Envoi de la requête DELETE.
        # reponse = requests.delete(url, headers=dict_en_tetes)
        reponse = urlfetch.fetch(url, method=urlfetch.DELETE,
                                 headers=dict_en_tetes,
                                 follow_redirects=False, deadline=None)

        # Création d'un dictionnaire contenant toute l'information
        # sur le test (requête, réponse et résultat).
        dict_test = self.creer_dict_test(titre, url, methode,
                                         dict_en_tetes, contenu_json,
                                         list_codes_statut, reponse)

        # Ajout du test à la liste de tests.
        list_tests.append(dict_test)

        #############################################
        # TEST-6 : Consulter un jeu supprimé #
        #############################################
        titre = u'TEST-6 : Consulter un jeu supprimé'

        # Informations pour créer la requête.
        url = url_base + '/game/2424'
        methode = 'GET'
        dict_en_tetes = {'Accept': 'application/json'}
        dict_contenu = None
        # Conversion du dictionnaire en chaîne JSON.
        contenu_json = json.dumps(dict_contenu, default=serialiser_pour_json)

        # Codes de statut de la réponse signifiant un succès.
        list_codes_statut = [404]

        # Envoi de la requête GET.
        # reponse = requests.get(url, headers=dict_en_tetes)
        reponse = urlfetch.fetch(url, method=urlfetch.GET,
                                 headers=dict_en_tetes,
                                 follow_redirects=False, deadline=None)

        # Création d'un dictionnaire contenant toute l'information
        # sur le test (requête, réponse et résultat).
        dict_test = self.creer_dict_test(titre, url, methode,
                                         dict_en_tetes, contenu_json,
                                         list_codes_statut, reponse)

        # Ajout du test à la liste de tests.
        list_tests.append(dict_test)


        ###########################################
        # TEST-2 : Consulter une personne ajoutée #
        ###########################################
        titre = u'TEST-2 : Consulter une personne ajoutée'

        # Informations pour créer la requête.
        url = url_base + '/personnes/123-456-789'
        methode = 'GET'
        dict_en_tetes = {'Accept': 'application/json'}
        dict_contenu = None
        # Conversion du dictionnaire en chaîne JSON.
        contenu_json = json.dumps(dict_contenu, default=serialiser_pour_json)

        # Codes de statut de la réponse signifiant un succès.
        list_codes_statut = [200]

        # Envoi de la requête GET.
        # reponse = requests.get(url, headers=dict_en_tetes)
        reponse = urlfetch.fetch(url, method=urlfetch.GET, 
                                 headers=dict_en_tetes,
                                 follow_redirects=False, deadline=None)

        # Création d'un dictionnaire contenant toute l'information
        # sur le test (requête, réponse et résultat).
        dict_test = self.creer_dict_test(titre, url, methode,
                                         dict_en_tetes, contenu_json,
                                         list_codes_statut, reponse)

        # Ajout du test à la liste de tests.
        list_tests.append(dict_test)

        ##################################
        # TEST-3 : Modifier une personne #
        ##################################
        titre = u'TEST-3 : Modifier une personne'

        # Informations pour créer la requête.
        url = url_base + '/personnes/123-456-789'
        methode = 'PUT'
        dict_en_tetes = {'Content-Type': 'application/json; charset=utf-8',
                         'Accept': 'application/json'}

        dict_contenu = {'nom': 'pierrette malette', 'age': 33}
        # Conversion du dictionnaire en chaîne JSON.
        contenu_json = json.dumps(dict_contenu, default=serialiser_pour_json)

        # Codes de statut de la réponse signifiant un succès.
        list_codes_statut = [200]

        # Envoi de la requête PUT.
        # reponse = requests.put(url, headers=dict_en_tetes, data=contenu_json)
        reponse = urlfetch.fetch(url, payload=contenu_json,
                                 method=urlfetch.PUT, 
                                 headers=dict_en_tetes,
                                 follow_redirects=False, deadline=None)

        # Création d'un dictionnaire contenant toute l'information
        # sur le test (requête, réponse et résultat).
        dict_test = self.creer_dict_test(titre, url, methode,
                                         dict_en_tetes, contenu_json,
                                         list_codes_statut, reponse)

        # Ajout du test à la liste de tests.
        list_tests.append(dict_test)

        ############################################
        # TEST-4 : Consulter une personne modifiée #
        ############################################
        titre = u'TEST-4 : Consulter une personne modifiée'

        # Informations pour créer la requête.
        url = url_base + '/personnes/123-456-789'
        methode = 'GET'
        dict_en_tetes = {'Accept': 'application/json'}
        dict_contenu = None
        # Conversion du dictionnaire en chaîne JSON.
        contenu_json = json.dumps(dict_contenu, default=serialiser_pour_json)

        # Codes de statut de la réponse signifiant un succès.
        list_codes_statut = [200]

        # Envoi de la requête GET.
        # reponse = requests.get(url, headers=dict_en_tetes)
        reponse = urlfetch.fetch(url, method=urlfetch.GET,
                                 headers=dict_en_tetes,
                                 follow_redirects=False, deadline=None)

        # Création d'un dictionnaire contenant toute l'information
        # sur le test (requête, réponse et résultat).
        dict_test = self.creer_dict_test(titre, url, methode,
                                         dict_en_tetes, contenu_json,
                                         list_codes_statut, reponse)

        # Ajout du test à la liste de tests.
        list_tests.append(dict_test)

        ###################################
        # TEST-5 : Supprimer une personne #
        ###################################
        titre = u'TEST-5 : Supprimer une personne'

        # Informations pour créer la requête.
        url = url_base + '/personnes/123-456-789'
        methode = 'DELETE'
        dict_en_tetes = {}
        dict_contenu = None
        # Conversion du dictionnaire en chaîne JSON.
        contenu_json = json.dumps(dict_contenu, default=serialiser_pour_json)

        # Codes de statut de la réponse signifiant un succès.
        list_codes_statut = [200, 204]

        # Envoi de la requête DELETE.
        # reponse = requests.delete(url, headers=dict_en_tetes)
        reponse = urlfetch.fetch(url, method=urlfetch.DELETE,
                                 headers=dict_en_tetes,
                                 follow_redirects=False, deadline=None)

        # Création d'un dictionnaire contenant toute l'information
        # sur le test (requête, réponse et résultat).
        dict_test = self.creer_dict_test(titre, url, methode,
                                         dict_en_tetes, contenu_json,
                                         list_codes_statut, reponse)

        # Ajout du test à la liste de tests.
        list_tests.append(dict_test)

        #############################################
        # TEST-6 : Consulter une personne supprimée #
        #############################################
        titre = u'TEST-6 : Consulter une personne supprimée'

        # Informations pour créer la requête.
        url = url_base + '/personnes/123-456-789'
        methode = 'GET'
        dict_en_tetes = {'Accept': 'application/json'}
        dict_contenu = None
        # Conversion du dictionnaire en chaîne JSON.
        contenu_json = json.dumps(dict_contenu, default=serialiser_pour_json)

        # Codes de statut de la réponse signifiant un succès.
        list_codes_statut = [404]

        # Envoi de la requête GET.
        # reponse = requests.get(url, headers=dict_en_tetes)
        reponse = urlfetch.fetch(url, method=urlfetch.GET,
                                 headers=dict_en_tetes,
                                 follow_redirects=False, deadline=None)

        # Création d'un dictionnaire contenant toute l'information
        # sur le test (requête, réponse et résultat).
        dict_test = self.creer_dict_test(titre, url, methode,
                                         dict_en_tetes, contenu_json,
                                         list_codes_statut, reponse)

        # Ajout du test à la liste de tests.
        list_tests.append(dict_test)

        #################
        # FIN DES TESTS #
        #################

        # Ce code permet de visualiser le code JSON pour la liste des tests.
        # Conversion du dictionnaire en chaîne JSON.
        # tests_json = json.dumps(list_tests, default=serialiser_pour_json)
        # self.response.out.write(tests_json)

        # Appel du template qui génère le HTML et écriture de la réponse.
        self.response.out.write(template.render({'data': list_tests})
                                .encode('utf-8'))

        #######################
        # SUITE DES TESTS ICI #
        #######################

        # ATTENTION : Pour une requête POST, n'oubliez pas d'afficher
        # l'en-tête "Location" de la réponse HTTP; bien sûr, vous devrez
        # modifier les méthodes et templates fournis.


app = webapp2.WSGIApplication(
    [
        webapp2.Route(r'/',
                      handler=MainPageHandler,
                      methods=['GET']),
    ],
    debug=True)
